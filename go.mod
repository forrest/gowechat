module gowechat

go 1.16

require (
	github.com/astaxie/beego v1.12.3
	github.com/gin-gonic/gin v1.7.2
	github.com/yaotian/gowechat v0.0.0-20180129120839-42de4f86cb0f
)